//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res) {
  //res.send("hola mundo desde Node.js");
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/',function(req, res) {
  res.send("peticion post");
})

app.put('/',function(req, res) {
  res.send("peticion put");
})

app.delete('/',function(req, res) {
  res.send("peticion delete");
})

app.get('/Clientes/:idcliente',function(req, res) {
  res.send("aqui tiene al cliente: "+req.params.idcliente);
})

app.get('/v1/Movimientos',function(req,res) {
  res.sendfile('movimientosv1.json')
})

app.get('/v2/Movimientos',function(req,res) {
  res.json(movimientosJSONv2);
})

app.get('/v2/Movimientos/:id',function(req, res) {
  console.log(req.params.id);
  res.send(movimientosJSONv2[req.params.id-1]);
})

app.get('/v2/Movimientos/:id/:nombre',function(req, res) {
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
})

app.get('/v2/Movimientosq',function(req, res) {
  console.log(req.query);
  res.send("Peticion con query recibida y cambiada: "+req.query);
})
